// LoginRequest
package fgoNetwork


import (
	"fmt"
	"../simplejson"
	"../account"
	"net/http"
    "net/url"
	"strings"
	"strconv"
    //"io/ioutil"
	"net/http/httputil"
	
)


const (
	//LoginUrl="/login/top?_userId="
)

type FgoLoginRequest struct{
	FgoRequest
	url string
}


func (fgoReq *FgoLoginRequest)Init(user account.AccountInfo){
	userAccount=user
	fgoReq.method="POST"
	fgoReq.url="/login/top?_userId="
}

func (fgoReq *FgoLoginRequest)Request() account.AccountInfo{
	//1, Get userAccount info
	//userAccount=user
	//2,set request body parameters
	data :=url.Values{}
	data=fgoReq.setBody()
	//3,generate a request with url and method
    req, _ := http.NewRequest(
        fgoReq.method,
        BaseUrl+fgoReq.url+userAccount.Uid,
        strings.NewReader(data.Encode()),
     )
	//4,set headers
    fgoReq.setHeader(req,false)//no cookies
	//print request
	reqs,_:=httputil.DumpRequest(req,true)
	fmt.Printf("request %s\n\n\n ",reqs)
	//5,get response
	resp, _ := getBody(req)
	respCookies :=resp.Cookies()
    defer resp.Body.Close()
	//json
	rootObject, _ :=simplejson.NewFromReader(resp.Body)
	//check if error
	resCode,_:=rootObject.Get("response").GetIndex(0).Get("resCode").String()
	isError :=showError(resCode,rootObject,reqs)
	if !isError{
		fmt.Printf("Login ok \n ")
		
	}
	
	
	//set Deck
	deckId,_:=rootObject.Get("cache").Get("replaced").Get("userGame").GetIndex(0).Get("activeDeckId").Int()
	userAccount.DeckId=strconv.Itoa(deckId)
		
	//set cookies	
	cookieString:=fmt.Sprintf("%s",respCookies)
	cookieString=strings.Trim(cookieString, "]")
	cookieString=strings.Trim(cookieString, "[")
	userAccount.Cookie=cookieString
	fmt.Printf("cookies: %s\n ",userAccount.Cookie)
	return userAccount
	
}

func LoginTest() {
	fmt.Println("Hello World!")
}
