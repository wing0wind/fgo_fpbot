// FriendOfferRequest
package fgoNetwork

import (
	"fmt"
	"../account"
	"../simplejson"
	"net/http"
    "net/url"
	"strings"
    "io/ioutil"
	"net/http/httputil"
	"io"
)


const(
	pTargetFriendUid="targetUserId"
)

type FgoFriendOfferRequest struct{
	FgoRequest
	url string
	//body
}

func (fgoReq *FgoFriendOfferRequest)Init(user account.AccountInfo){
	userAccount=user
	fgoReq.method="POST"
	fgoReq.url="/friend/offer?_userId="
}

func (fgoReq *FgoFriendOfferRequest)setBody(targetFriendUid string) (data url.Values){
	appVer:=testAppVerAndroid
	if userAccount.IsiOS{
		appVer=testAppVeriOS
	}
	
	data = url.Values{  
						pUid: {userAccount.Uid},
						pAuthKey:{userAccount.AuthKey},
						pAppVer:{appVer},
						pDataVer:{testDataVer},
						pLastAcsTime:{getTime()},
						pTargetFriendUid:{targetFriendUid},
												}
	nAuthCode:=	userAccount.CalcAuthCode(Encode(data))						
	data.Add(pAuthCode,nAuthCode)
	
	return
}

func (fgoReq *FgoFriendOfferRequest)getDataFromJson(r io.Reader){
	//json
	rootObject, _ :=simplejson.NewFromReader(r)
	fmt.Printf("Json: ")
	fmt.Println(rootObject.Get("cache").Get("updated"))
	fmt.Printf("json\n ")
}

func (fgoReq *FgoFriendOfferRequest)Request(targetFriendUid string){
	
	//2,set request body parameters
	data :=url.Values{}
	data=fgoReq.setBody(targetFriendUid)
	//3,generate a request with url and method
    req, _ := http.NewRequest(
        fgoReq.method,
        BaseUrl+fgoReq.url+userAccount.Uid,
        strings.NewReader(data.Encode()),
     )
	//4,set headers
	//false :no cookies 
    fgoReq.setHeader(req,true)

	//print request
	reqs,_:=httputil.DumpRequest(req,true)
	
	//5,get response
	resp, _ := getBody(req)
	respHead := resp.Header
    respBody, _ :=ioutil.ReadAll(resp.Body)
    defer resp.Body.Close()
	rootObject, _ :=simplejson.NewFromReader(resp.Body)
	//check if error
	resCode,_:=rootObject.Get("response").GetIndex(0).Get("resCode").String()
	isError :=showError(resCode,rootObject,reqs)
	if !isError{
		fmt.Printf("Friend ok \n ")
	}
	
	fmt.Printf("head: %s\n ",respHead)
    println(string(respBody)+"\n")
}


