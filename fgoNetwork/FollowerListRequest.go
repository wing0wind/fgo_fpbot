// FollowerListRequest
package fgoNetwork


import (
	"fmt"
	"../simplejson"
	"../account"
	"net/http"
    "net/url"
	"strings"
    //"io/ioutil"
	"net/http/httputil"
	
)

type FgoFollowerListRequest struct{
	FgoRequest
	url string
}


func (fgoReq *FgoFollowerListRequest)Init(user account.AccountInfo){
	userAccount=user
	fgoReq.method="POST"
	fgoReq.url="/follower/list?_userId="
}

func (fgoReq *FgoFollowerListRequest)Request() account.AccountInfo{
	//1, Get userAccount info
	//userAccount=user
	//2,set request body parameters
	data :=url.Values{}
	data=fgoReq.setBody()
	//3,generate a request with url and method
    req, _ := http.NewRequest(
        fgoReq.method,
        BaseUrl+fgoReq.url+userAccount.Uid,
        strings.NewReader(data.Encode()),
     )
	//4,set headers
	//cookies
    fgoReq.setHeader(req,true)
	//print request
	reqs,_:=httputil.DumpRequest(req,true)
	//5,get response
	resp, _ := getBody(req)
    defer resp.Body.Close()
	//json
	rootObject, _ :=simplejson.NewFromReader(resp.Body)
	//check if error
	resCode,_:=rootObject.Get("response").GetIndex(0).Get("resCode").String()
	isError :=showError(resCode,rootObject,reqs)
	//fmt.Printf("Json: %s\n",resCode)
	if !isError{
		fmt.Printf("Get Follower ok \n ")
	}
		
	return userAccount
	
}

