// GaChaRequest
package fgoNetwork

import (
	"fmt"
	"../account"
	"../simplejson"
	"net/http"
    "net/url"
	"strings"
    "io/ioutil"
	"net/http/httputil"
	"io"
)


const(
	pGachaId="gachaId"
    pGachaNum="num"
    pTicketItemId="ticketItemId"
    pShopIdIdx="shopIdIndex"	
	)
	


type FgoGaChaRequest struct{
	FgoRequest
	url string
	//body
}

func (fgoReq *FgoGaChaRequest)Init(user account.AccountInfo){
	userAccount=user
	fgoReq.method="POST"
	fgoReq.url="/gacha/draw"
}

func (fgoReq *FgoGaChaRequest)setBody(gachaId,num,ticketItemId,shopIdIdx string) (data url.Values){
	appVer:=testAppVerAndroid
	if userAccount.IsiOS{
		appVer=testAppVeriOS
	}
	
	data = url.Values{  
						pUid: {userAccount.Uid},
						pAuthKey:{userAccount.AuthKey},
						pAppVer:{appVer},
						pDataVer:{testDataVer},
						pLastAcsTime:{getTime()},
						pGachaId:{gachaId},
    					pGachaNum:{num},
    					pTicketItemId:{ticketItemId},
    					pShopIdIdx:{shopIdIdx},
												}
	nAuthCode:=	userAccount.CalcAuthCode(Encode(data))	
	fmt.Printf("data: %s\n ",Encode(data))									
	data.Add(pAuthCode,nAuthCode)
	
	return
}

func (fgoReq *FgoGaChaRequest)getDataFromJson(r io.Reader){
	//json
	rootObject, _ :=simplejson.NewFromReader(r)
	fmt.Printf("Json: ")
	fmt.Println(rootObject.Get("cache").Get("updated"))
	fmt.Printf("json\n ")
	
}

func (fgoReq *FgoGaChaRequest)Request(gachaId,num,ticketItemId,shopIdIdx string){
	
	//2,set request body parameters
	data :=url.Values{}
	data=fgoReq.setBody(gachaId,num,ticketItemId,shopIdIdx)
	//3,generate a request with url and method
    req, _ := http.NewRequest(
        fgoReq.method,
        BaseUrl+fgoReq.url,
        strings.NewReader(data.Encode()),
     )
	//4,set headers
	//false :no cookies 
    fgoReq.setHeader(req,true)

	//print request
	reqs,_:=httputil.DumpRequest(req,true)
	fmt.Printf("%s\n ",reqs)
	//5,get response
	client := &http.Client{}
	resp, _ := client.Do(req)
	respHead := resp.Header
    respBody, _ :=ioutil.ReadAll(resp.Body)
    defer resp.Body.Close()
	
	fmt.Printf("Head: %s\n ",respHead)
    println(string(respBody)+"\n")
}


