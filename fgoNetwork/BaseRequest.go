// LoginRequest
package fgoNetwork

import (
	"fmt"
	"../account"
	"net/http"
    "net/url"
	"bytes"
	"sort"
	"time"
	"strconv"
	//"strings"
    //"io/ioutil"
	//"net/http/httputil"
)

const (
	UAAndroid   = "Dalvik/1.6.0 (Linux; U; Android 4.4.2; GT-N7100 Build/KOT49H)"
	UAiOS="Mozilla/5.0 (iPhone; CPU iPhone OS 8_1_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12B466"
	BaseUrl="http://game.fate-go.jp"
	
	pUid="userId"
	pAuthKey="authKey"
	pAppVer="appVer"
	pDataVer="dataVer"
	pLastAcsTime="lastAccessTime"
	pAuthCode="authCode"
	//test
	testAppVerAndroid="1.0.4"
	testAppVeriOS="1.1.0"
	testDataVer = "35"
	testLastAcstime="1442012415"
)

type FgoRequest struct{
	method string
}

var userAccount account.AccountInfo

func assert(data interface{}) {
    switch data.(type) {
    case string:
        fmt.Print(data.(string))
    case float64:
        fmt.Print(data.(float64))
    case bool:
        fmt.Print(data.(bool))
    case nil:
        fmt.Print("null")
    case []interface{}:
        fmt.Print("[")
        for _, v := range data.([]interface{}) {
            assert(v)
            fmt.Print(" ")
        }
        fmt.Print("]")
    case map[string]interface{}:
        fmt.Print("{")
        for k, v := range data.(map[string]interface{}) {
            fmt.Print(k + ":")
            assert(v)
            fmt.Print(" ")
        }
        fmt.Print("}")
    default:
    }
}

func (fgoReq *FgoRequest)setBody() (data url.Values){
	appVer:=testAppVerAndroid
	if userAccount.IsiOS{
		appVer=testAppVeriOS
	}
	
	data = url.Values{  pUid: {userAccount.Uid},
						pAuthKey:{userAccount.AuthKey},
						pAppVer:{appVer},
						pDataVer:{testDataVer},
						pLastAcsTime:{getTime()},
												}
	nAuthCode:=	userAccount.CalcAuthCode(Encode(data))								
	data.Add(pAuthCode,nAuthCode)
	
	return
}

func (fgoReq *FgoRequest)setHeader(req *http.Request,hasCookie bool){
	//req.Header.Set("Accept-Encoding", "gzip")
	UAString:=UAAndroid
	if userAccount.IsiOS{
		UAString=UAiOS
	}
	req.Header.Set("User-Agent",UAString)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("X-Unity-Version", "4.6.7p1")
	if hasCookie{
		req.Header.Set("Cookie" ,userAccount.Cookie)
	}
	//println(fgoReq.method)//it show post,so good
}

func getBody(requset *http.Request)(resp *http.Response, err error){
	//5,get response
	client := &http.Client{}
	resp, err = client.Do(requset)
	for err != nil {
		fmt.Println(err)
		time.Sleep(5 * time.Second)
    	resp, err = client.Do(requset)
	}
	return
}

func getTime() string{
	time:=int(time.Now().In(time.FixedZone("Asia/Tokyo", 9*60*60)).Unix())
	return  strconv.Itoa(time)
}

func showError(errCode string,request,response interface{}) bool{
	errInt,_:=strconv.Atoi(errCode)
	if errInt>0{
		fmt.Printf("request %s\n\n\n ",request)
		fmt.Printf("Json: ")
		fmt.Println(response)
		fmt.Printf("json\n ")
		return true
	}else{
		return false
	}
	
}

func Encode(v url.Values) string {
	if v == nil {
		return ""
	}
	var buf bytes.Buffer
	keys := make([]string, 0, len(v))
	for k := range v {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		vs := v[k]
		prefix := k + "="
		for _, v := range vs {
			if buf.Len() > 0 {
				buf.WriteByte('&')
			}
			buf.WriteString(prefix)
			buf.WriteString(v)
		}
	}
	return buf.String()
}

