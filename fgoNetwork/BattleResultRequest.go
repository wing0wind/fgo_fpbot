// LoginRequest
package fgoNetwork

import (
	"fmt"
	"../account"
	"../simplejson"
	"net/http"
    "net/url"
	"strings"
    //"io/ioutil"
	"net/http/httputil"
	"io"
)


const(
	pBattleId="battleId"
	pBattleResult="battleResult"
    pScores="score"
    pAction="action"
	testAction="{ logs:[{\"uid\":1,\"ty\":2},{\"uid\":1,\"ty\":3},{\"uid\":1,\"ty\":5},{\"uid\":1,\"ty\":1},{\"uid\":1,\"ty\":1},{\"uid\":1,\"ty\":2},{\"uid\":1,\"ty\":2},{\"uid\":1,\"ty\":1},{\"uid\":1,\"ty\":3}]}"
)
	


type FgoBattleResultRequest struct{
	FgoRequest
	url string
	//body
}

func (fgoReq *FgoBattleResultRequest)Init(user account.AccountInfo){
	userAccount=user
	fgoReq.method="POST"
	fgoReq.url="/battle/result?_userId="
}

func (fgoReq *FgoBattleResultRequest)setBody(battleId,battleResult,scores,action string) (data url.Values){
	appVer:=testAppVerAndroid
	if userAccount.IsiOS{
		appVer=testAppVeriOS
	}
	
	data = url.Values{  
						pUid: {userAccount.Uid},
						pAuthKey:{userAccount.AuthKey},
						pAppVer:{appVer},
						pDataVer:{testDataVer},
						pLastAcsTime:{getTime()},
						pBattleId:{battleId},
    					pBattleResult:{battleResult},
    					pScores:{scores},
    					pAction:{testAction},
												}
	nAuthCode:=	userAccount.CalcAuthCode(Encode(data))										
	data.Add(pAuthCode,nAuthCode)
	
	return
}

func (fgoReq *FgoBattleResultRequest)getDataFromJson(r io.Reader){
	//json
	rootObject, _ :=simplejson.NewFromReader(r)
	fmt.Printf("Json: ")
	fmt.Println(rootObject.Get("cache").Get("updated"))
	fmt.Printf("json\n ")
	
}

func (fgoReq *FgoBattleResultRequest)Request(battleId,battleResult,scores,action string){
	
	//2,set request body parameters
	data :=url.Values{}
	data=fgoReq.setBody(battleId,battleResult,scores,action)
	//3,generate a request with url and method
    req, _ := http.NewRequest(
        fgoReq.method,
        BaseUrl+fgoReq.url+userAccount.Uid,
        strings.NewReader(data.Encode()),
     )
	//4,set headers
	//false :no cookies 
    fgoReq.setHeader(req,true)

	//print request
	reqs,_:=httputil.DumpRequest(req,true)
	
	//5,get response
	resp, _ := getBody(req)
    defer resp.Body.Close()
	rootObject, _ :=simplejson.NewFromReader(resp.Body)
	//check if error
	resCode,_:=rootObject.Get("response").GetIndex(0).Get("resCode").String()
	isError :=showError(resCode,rootObject,reqs)
	if !isError{
		fmt.Printf("Battle Result ok \n ")
	}
	
}


