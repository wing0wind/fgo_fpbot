// LoginRequest
package fgoNetwork

import (
	"fmt"
	"../account"
	"../simplejson"
	"net/http"
    "net/url"
	"strings"
	"net/http/httputil"
	"io"
	"strconv"
	//"io/ioutil"
)


const(
	pQuestId="questId"
    pQuestPhase="questPhase"
    pActiveDeckId="activeDeckId"
    pFollowerId="followerId"
)
	


type FgoBattleSetupRequest struct{
	FgoRequest
	url string
	//body
}

func (fgoReq *FgoBattleSetupRequest)Init(user account.AccountInfo){
	userAccount=user
	fgoReq.method="POST"
	fgoReq.url="/battle/setup?_userId="
}

func (fgoReq *FgoBattleSetupRequest)setBody(questId,questPhase,activeDeckId,followerId string) (data url.Values){
	appVer:=testAppVerAndroid
	if userAccount.IsiOS{
		appVer=testAppVeriOS
	}
	
	data = url.Values{  pUid: {userAccount.Uid},
						pAuthKey:{userAccount.AuthKey},
						pAppVer:{appVer},
						pDataVer:{testDataVer},
						pLastAcsTime:{getTime()},
						pQuestId:{questId},
    					pQuestPhase:{questPhase},
    					pActiveDeckId:{activeDeckId},
    					pFollowerId:{followerId},
												}
	nAuthCode:=	userAccount.CalcAuthCode(Encode(data))										
	data.Add(pAuthCode,nAuthCode)
	
	return
}

func (fgoReq *FgoBattleSetupRequest)getDataFromJson(r io.Reader){
	//json
	rootObject, _ :=simplejson.NewFromReader(r)
	fmt.Printf("Json: ")
	fmt.Println(rootObject.Get("cache"))
	fmt.Printf("json\n ")
	fmt.Println(rootObject.Get("cache").Get("replaced"))
	fmt.Printf("json\n ")
	fmt.Println(rootObject.Get("cache").Get("replaced").Get("battle"))
	fmt.Printf("json\n ")
	fmt.Println(rootObject.Get("cache").Get("replaced").Get("battle").GetIndex(0))
	fmt.Printf("json\n ")
	fmt.Println(rootObject.Get("cache").Get("replaced").Get("battle").GetIndex(0).Get("id"))
	fmt.Printf("json\n ")
	fmt.Println(rootObject.Get("cache").Get("replaced").Get("battle").GetIndex(0).Get("id").Int())
	fmt.Printf("json\n ")
	
}

func (fgoReq *FgoBattleSetupRequest)Request(questId,questPhase,activeDeckId,followerId string) account.AccountInfo{
	//2,set request body parameters
	data :=url.Values{}
	data=fgoReq.setBody(questId,questPhase,activeDeckId,followerId)
	//3,generate a request with url and method
    req, _ := http.NewRequest(
        fgoReq.method,
        BaseUrl+fgoReq.url+userAccount.Uid,
        strings.NewReader(data.Encode()),
     )
	//4,set headers
	//false :no cookies 
    fgoReq.setHeader(req,true)

	//print request
	reqs,_:=httputil.DumpRequest(req,true)
	//5,get response
	resp, _ := getBody(req)
    defer resp.Body.Close()
	rootObject, _ :=simplejson.NewFromReader(resp.Body)
	//check if error
	resCode,_:=rootObject.Get("response").GetIndex(0).Get("resCode").String()
	isError :=showError(resCode,rootObject,reqs)
	if !isError{
		fmt.Printf("Battle Setup ok \n ")
	}
	
	//get battleId
	battleId,_:=rootObject.Get("cache").Get("replaced").Get("battle").GetIndex(0).Get("id").Int()
	userAccount.TempBatteId=strconv.Itoa(battleId)
	
    return userAccount
}


