// fpbot
package main

import (
	"fmt"
	"./account"
	"./fgoNetwork"
	"time"
	//"net/http"
    //"net/url"
    //"strings"
    //"io/ioutil"
	//"net/http/httputil"
	//"bytes"
	//"sort"
	
)

var userAccount account.AccountInfo
var accountSlice []account.AccountInfo

const (
	UAAndroid   = "Dalvik/1.6.0 (Linux; U; Android 4.4.2; GT-N7100 Build/KOT49H)"
	UAiOS="Mozilla/5.0 (iPhone; CPU iPhone OS 8_1_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12B466"
	BaseUrl="http://game.fate-go.jp"
	Login="/login/top?_userId="
	pUid="userId"
	pAuthKey="authKey"
	pAppVer="appVer"
	pDataVer="dataVer"
	pLastAcsTime="lastAccessTime"
	pAuthCode="authCode"
	//test
	testAppVerAndroid="1.0.4"
	testAppVeriOS="1.0.1"
	testDataVer = "30"
	testLastAcstime="1442055836"
	//"1442012415"
	testFriendCode="999516460"
	testTargetUid="7929608"
)

func testCalc(){
	testString2:="appVer=1.0.1&authKey=FGRiQoeCcOi9KORN:5aPiAAAAAAA=&dataVer=30&lastAccessTime=1442012416&userId=14853093"
	userAccount.CalcAuthCode(testString2)
}

func makeFriendsRequest(){
	fgoFriendOfferReq:=&fgoNetwork.FgoFriendOfferRequest{}
	fgoFriendOfferReq.Init(userAccount)
	fgoFriendOfferReq.Request(testTargetUid)
	time.Sleep(1 * time.Second)
}

func LoginRequest(){
	fgoLoginReq:=&fgoNetwork.FgoLoginRequest{}
	fgoLoginReq.Init(userAccount)
	userAccount=fgoLoginReq.Request()//get cookie to userAccount
	time.Sleep(1 * time.Second)
	fmt.Println(time.Now())
}

func makeFriendLoop(){
	for i := 0; i < len(accountSlice); i++ {
		userAccount.GetAccountInfo(accountSlice[i])
		LoginRequest()
		makeFriendsRequest()
		time.Sleep(1 * time.Second)
	}
	
}

func battleRequest(){
	/*
	fgoHomeReq:=&fgoNetwork.FgoHomeTopRequest{}
	fgoHomeReq.Init(userAccount)
	userAccount=fgoHomeReq.Request()*/
	
	fgoFListReq:=&fgoNetwork.FgoFollowerListRequest{}
	fgoFListReq.Init(userAccount)
	userAccount=fgoFListReq.Request()
	time.Sleep(2 * time.Second)
	
	fgoBattleSetupReq:=&fgoNetwork.FgoBattleSetupRequest{}
	fgoBattleSetupReq.Init(userAccount)
	userAccount=fgoBattleSetupReq.Request("93000001", "3", userAccount.DeckId, testTargetUid)
	
	time.Sleep(1 * time.Second)
	/*
	fgoBattleResultReq:=&fgoNetwork.FgoBattleResultRequest{}
	fgoBattleResultReq.Init(userAccount)
	fgoBattleResultReq.Request(userAccount.TempBatteId, "1", "", "")
	time.Sleep(1 * time.Second)*/
	fmt.Println(time.Now())
}

func battleLoop(){
	for i := 0; i < len(accountSlice); i++ {
		userAccount.GetAccountInfo(accountSlice[i])
		fmt.Printf("Battle User %d proccess start .\n",i)
		LoginRequest()
		battleRequest()
		fmt.Printf("Battle User %d proccess over .\n",i)
	}
}

func mainBattleLoop(){
	battleLoop()
	fmt.Println("Wait fot next loop.\n")
	var iCount64 int64
	iCount64=1
	c1 := time.Tick(15 * time.Minute)
    for t := range c1 {
		fmt.Println(t)
        battleLoop()
		fmt.Printf("Battle %d loop over.Wait for next loop.\n",iCount64)
		iCount64=iCount64+1
    }
}

func main() {
	fmt.Println("Hello World!")
	accountSlice=account.GenerateAcSlice()
	//makeFriendLoop()
	
	//battleLoop()
	mainBattleLoop()
	
	var str string
    fmt.Scan(&str)
}

//search friends by f code
	//fgoFriendReq:=&fgoNetwork.FgoFriendProfileRequest{}
	//fgoFriendReq.Init(userAccount)
	//fgoFriendReq.Request(testFriendCode)

//fgoTutorialSetReq:=&fgoNetwork.FgoTutorialSetRequest{}
	//fgoTutorialSetReq.Init(userAccount)
	//fgoTutorialSetReq.Request()
	
	//fgoGaChaReq:=&fgoNetwork.FgoGaChaRequest{}
	//fgoGaChaReq.Init(userAccount)
	//fgoGaChaReq.Request("101", "1", "0", "1")