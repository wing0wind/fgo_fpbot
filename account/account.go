// account
package account

import (
	"fmt"
	"crypto/sha1"
	"encoding/base64"
)

const (
	testUid="8013633"
testAuthKey="xL34HAGuc5flRyhv:QUd6AAAAAAA="
testSecertKey="ag+yaxHI8NST9iXe:QUd6AAAAAAA="
	testDevice=true
	testCookie="ASP.NET_SessionId=pcymwp4pu01u1ogmr2vpwmod; Path=/; HttpOnly"
	)
	

type AccountInfo struct{
	Uid,AuthKey,SecertKey,Cookie,AuthCode,DeckId,TempBatteId string
	IsiOS bool
}

func Test(s string) {
	fmt.Println("Hello World!")
}

func GenerateAcSlice()(acSlice []AccountInfo){
	acSlice=[]AccountInfo{
		{"8013730","02lgBUV9jYg+xTk3:okd6AAAAAAA=","WO9cvtR2hlSZM48C:okd6AAAAAAA=","","","","",true},
		{"8013701","O7W7B1O56X0IRzOZ:hUd6AAAAAAA=","4U2ntLpM2P7uMD8j:hUd6AAAAAAA=","","","","",true},
		{"8013645","kBnFQOFJtBGdrqss:TUd6AAAAAAA=","SMAD7CtgnVzGnoAB:TUd6AAAAAAA=","","","","",true},
		{"8013633","xL34HAGuc5flRyhv:QUd6AAAAAAA=","ag+yaxHI8NST9iXe:QUd6AAAAAAA=","","","","",true},
		{"8013612","hCQ2aXzqNBbpaFTo:LEd6AAAAAAA=","1ejMgtBgdFmGtOzq:LEd6AAAAAAA=","","","","",true},
		{"8013541","8JTFXWZ6Z9LIOQF1:5UZ6AAAAAAA=","RxbBe92LN2001zKY:5UZ6AAAAAAA=","","","","",true},
		{"8013534","QH0fn5iY+J/0fKhl:3kZ6AAAAAAA=","k9HUDiDKJxX2kdU2:3kZ6AAAAAAA=","","","","",true},
		{"8013457","/SEDWUfQTMQe0MZl:kUZ6AAAAAAA=","ECro5JSDaFCXvZVU:kUZ6AAAAAAA=","","","","",true},
		{"8013398","cqycqTgXPBDHQcQq:VkZ6AAAAAAA=","ltCrn9uwLxEgLymO:VkZ6AAAAAAA=","","","","",true},//9
		{"8013381","Vr57H+3Df3bEvgqp:RUZ6AAAAAAA=","AVEXNBbB2CYx/4Ht:RUZ6AAAAAAA=","","","","",true},//10
	}
	return
	
}

func (accountInfo *AccountInfo) CalcAuthCode(requsetParameters string) (authCode string){
	if accountInfo.SecertKey!=""{
		data := []byte(requsetParameters+":"+accountInfo.SecertKey)
		hasher := sha1.New()
    	hasher.Write(data)
		authCode= base64.StdEncoding.EncodeToString(hasher.Sum(nil))
		fmt.Printf("acode:%s\n",authCode)
	}else{
		authCode=""
	}
	
	return
}



func (accountInfo *AccountInfo) GetAccountInfoTemp(b bool){
		accountInfo.Uid=testUid
		accountInfo.SecertKey=testSecertKey
		accountInfo.AuthKey=testAuthKey
		accountInfo.IsiOS=testDevice
}

func (accountInfo *AccountInfo) GetAccountInfo(acSliceMember AccountInfo){
		accountInfo.Uid=acSliceMember.Uid
		accountInfo.SecertKey=acSliceMember.SecertKey
		accountInfo.AuthKey=acSliceMember.AuthKey
		accountInfo.IsiOS=acSliceMember.IsiOS
		//fmt.Println(acSliceMember)
		//accountInfo=&acSliceMember
		//fmt.Println(accountInfo)
}
